// 统一请求路径前缀在libs/axios.js中修改
import { getRequest, postRequest, putRequest, deleteRequest, importRequest, uploadFileRequest, base } from '@/libs/axios';

// Vaptcha ID
export const vaptchaID = "5bb9a277fc650e00a4155567"
// 文件上传接口
export const uploadFile = base + "/upload/file"
// 自定义上传接口
export const fileUpload = base + "/file/upload"
// 验证码渲染图片接口
export const drawCodeImage = base + "/common/captcha/draw/"
// 获取菜单
export const getMenuList = base + "/permission/getMenuList"
// 获取数据字典
export const getDictData = base + "/dictData/getByType/"
// Websocket
export const ws = base + "/ws"



// let url = "http://10.0.254.72:8090"
// // Vaptcha ID
// export const vaptchaID = "5bb9a277fc650e00a4155567"
// // 文件上传接口
// export const uploadFile = url + "/gboot/upload/file"
// //自定义上传接口
// export const fileUpload = url + "/gboot/file/upload"
// // 验证码渲染图片接口
// export const drawCodeImage =url + "/gboot/common/captcha/draw/"
// // 获取菜单
// export const getMenuList = url + "/gboot/permission/getMenuList"
// // 获取数据字典
// export const getDictData = url +  "/gboot/dictData/getByType/"
// // Websocket
// export const ws = url + "/gboot/ws"



// 登陆
export const login = (params) => {
    return postRequest('/login', params)
}
// 获取用户登录信息
export const userInfo = (params) => {
    return getRequest('/user/info', params)
}
// 注册
export const regist = (params) => {
    return postRequest('/user/regist', params)
}
// 初始化验证码
export const initCaptcha = (params) => {
    return getRequest('/common/captcha/init', params)
}
// 发送短信验证码
export const sendSms = (mobile, params) => {
    return getRequest(`/common/captcha/sendSms/${mobile}`, params)
}
// 发送重置密码短信验证码
export const sendResetSms = (params) => {
    return postRequest('/common/captcha/sendResetSms', params)
}
// 通过手机重置密码
export const resetByMobile = (params) => {
    return postRequest('/user/resetByMobile', params)
}
// 发送重置密码邮件验证码
export const sendResetEmail = (params) => {
    return postRequest('/email/sendResetCode', params)
}
// 通过邮件重置密码
export const resetByEmail = (params) => {
    return postRequest('/email/resetByEmail', params)
}
// 短信验证码登录
export const smsLogin = (params) => {
    return postRequest('/user/smsLogin', params)
}
// IP天气信息
export const ipInfo = (params) => {
    return getRequest('/common/ip/info', params)
}
// 首页右侧顶部数据列表
export const firstPageTodayStatistics = (params) => {
    return getRequest('/firstPage/todayStatistics', params)
}
// 首页右侧本周已扣款金额趋势
export const firstPageWeekAmount = (params) => {
    return getRequest('/firstPage/weekAmount', params)
}
// 首页右侧本周已扣款订单量趋势
export const firstPageWeekCount = (params) => {
    return getRequest('/firstPage/weekCount', params)
}
// 个人中心编辑
export const userInfoEdit = (params) => {
    return postRequest('/user/edit', params)
}
// 个人中心发送修改邮箱验证邮件
export const sendCodeEmail = (email, params) => {
    return getRequest(`/email/sendCode/${email}`, params)
}
// 个人中心发送修改邮箱验证邮件
export const editEmail = (params) => {
    return postRequest('/email/editEmail', params)
}
// 个人中心修改密码
export const changePass = (params) => {
    return postRequest('/user/modifyPass', params)
}
// 个人中心修改手机
export const changeMobile = (params) => {
    return postRequest('/user/changeMobile', params)
}
// 解锁
export const unlock = (params) => {
    return postRequest('/user/unlock', params)
}



// github登录
export const githubLogin = (params) => {
    return getRequest('/social/github/login', params)
}
// qq登录
export const qqLogin = (params) => {
    return getRequest('/social/qq/login', params)
}
// 微博登录
export const weiboLogin = (params) => {
    return getRequest('/social/weibo/login', params)
}
// 绑定账号
export const relate = (params) => {
    return postRequest('/social/relate', params)
}
// 获取JWT
export const getJWT = (params) => {
    return getRequest('/social/getJWT', params)
}



// 获取绑定账号信息
export const relatedInfo = (username, params) => {
    return getRequest(`/relate/getRelatedInfo/${username}`, params)
}
// 解绑账号
export const unRelate = (params) => {
    return postRequest('/relate/delByIds', params)
}
// 分页获取绑定账号信息
export const getRelatedListData = (params) => {
    return getRequest('/relate/findByCondition', params)
}



// 获取用户数据 多条件
export const getUserListData = (params) => {
    return getRequest('/user/getByCondition', params)
}
// 获取用户管理业务方数据
export const getUserBizGetAll = (params) => {
    return getRequest('/biz/getAll', params)
}
// 通过用户名搜索
export const searchUserByName = (username, params) => {
    return getRequest('/user/searchByName/'+username, params)
}
// 获取全部用户数据
export const getAllUserData = (params) => {
    return getRequest('/user/getAll', params)
}
// 通过部门获取全部用户数据
export const getUserByDepartmentId = (id, params) => {
    return getRequest(`/user/getByDepartmentId/${id}`, params)
}
// 添加用户
export const addUser = (params) => {
    return postRequest('/user/admin/add', params)
}
// 编辑用户
export const editUser = (params) => {
    return postRequest('/user/admin/edit', params)
}
// 启用用户
export const enableUser = (id, params) => {
    return postRequest(`/user/admin/enable/${id}`, params)
}
// 禁用用户
export const disableUser = (id, params) => {
    return postRequest(`/user/admin/disable/${id}`, params)
}
// 重置密码
export const resetPass = (id, params) => {
    return getRequest(`/user/resetPass/${id}`, params)
}
// 删除用户
export const deleteUser = (ids, params) => {
    return deleteRequest(`/user/delByIds/${ids}`, params)
}
// 导入用户
export const importUserData = (params) => {
    return importRequest('/user/importData', params)
}



// 获取一级部门
export const initDepartment = (params) => {
    return getRequest('/department/getByParentId/0', params)
}
// 加载部门子级数据
export const loadDepartment = (id, params) => {
    return getRequest(`/department/getByParentId/${id}`, params)
}
// 添加部门
export const addDepartment = (params) => {
    return postRequest('/department/add', params)
}
// 编辑部门
export const editDepartment = (params) => {
    return postRequest('/department/edit', params)
}
// 删除部门
export const deleteDepartment = (ids, params) => {
    return deleteRequest(`/department/delByIds/${ids}`, params)
}
// 搜索部门
export const searchDepartment = (params) => {
    return getRequest('/department/search', params)
}



// 获取全部角色数据
export const getAllRoleList = (params) => {
    return getRequest('/role/getAllList', params)
}
// 分页获取角色数据
export const getRoleList = (params) => {
    return getRequest('/role/getAllByPage', params)
}
// 添加角色
export const addRole = (params) => {
    return postRequest('/role/save', params)
}
// 编辑角色
export const editRole = (params) => {
    return postRequest('/role/edit', params)
}
// 设为或取消注册角色
export const setDefaultRole = (params) => {
    return postRequest('/role/setDefault', params)
}
// 分配角色权限
export const editRolePerm = (params) => {
    return postRequest('/role/editRolePerm', params)
}
// 分配角色数据权限
export const editRoleDep = (params) => {
    return postRequest('/role/editRoleDep', params)
}
// 删除角色
export const deleteRole = (ids, params) => {
    return deleteRequest(`/role/delAllByIds/${ids}`, params)
}



// 获取全部权限数据
export const getAllPermissionList = (params) => {
    return getRequest('/permission/getAllList', params)
}
// 添加权限
export const addPermission = (params) => {
    return postRequest('/permission/add', params)
}
// 编辑权限
export const editPermission = (params) => {
    return postRequest('/permission/edit', params)
}
// 删除权限
export const deletePermission = (ids, params) => {
    return deleteRequest(`/permission/delByIds/${ids}`, params)
}
// 搜索权限
export const searchPermission = (params) => {
    return getRequest('/permission/search', params)
}



// 获取全部字典
export const getAllDictList = (params) => {
    return getRequest('/dict/getAll', params)
}
// 添加字典
export const addDict = (params) => {
    return postRequest('/dict/add', params)
}
// 编辑字典
export const editDict = (params) => {
    return postRequest('/dict/edit', params)
}
// 删除字典
export const deleteDict = (ids, params) => {
    return deleteRequest(`/dict/delByIds/${ids}`, params)
}
// 搜索字典
export const searchDict = (params) => {
    return getRequest('/dict/search', params)
}
// 获取全部字典数据
export const getAllDictDataList = (params) => {
    return getRequest('/dictData/getByCondition', params)
}
// 添加字典数据
export const addDictData = (params) => {
    return postRequest('/dictData/add', params)
}
// 编辑字典数据
export const editDictData = (params) => {
    return postRequest('/dictData/edit', params)
}
// 删除字典数据
export const deleteData = (ids, params) => {
    return deleteRequest(`/dictData/delByIds/${ids}`, params)
}
// 通过类型获取字典数据
export const getDictDataByType = (type, params) => {
    return getRequest(`/dictData/getByType/${type}`, params)
}



// 分页获取日志数据
export const getLogListData = (params) => {
    return getRequest('/log/getAllByPage', params)
}
// 删除日志
export const deleteLog = (ids, params) => {
    return deleteRequest(`/log/delByIds/${ids}`, params)
}
// 清空日志
export const deleteAllLog = (params) => {
    return deleteRequest('/log/delAll', params)
}


// 分页获取Redis数据
export const getRedisData = (params) => {
    return getRequest('/redis/getAllByPage', params)
}
// 通过key获取Redis信息
export const getRedisByKey = (key, params) => {
    return getRequest(`/redis/getByKey/${key}`, params)
}
// 获取Redis键值数量
export const getRedisKeySize = (params) => {
    return getRequest('/redis/getKeySize', params)
}
// 获取Redis内存
export const getRedisMemory = (params) => {
    return getRequest('/redis/getMemory', params)
}
// 获取Redis信息
export const getRedisInfo = (params) => {
    return getRequest('/redis/info', params)
}
// 添加编辑Redis
export const saveRedis = (params) => {
    return postRequest('/redis/save', params)
}
// 删除Redis
export const deleteRedis = (params) => {
    return deleteRequest('/redis/delByKeys', params)
}
// 清空Redis
export const deleteAllRedis = (params) => {
    return deleteRequest('/redis/delAll', params)
}



// 分页获取定时任务数据
export const getQuartzListData = (params) => {
    return getRequest('/quartzJob/getAllByPage', params)
}
// 添加定时任务
export const addQuartz = (params) => {
    return postRequest('/quartzJob/add', params)
}
// 编辑定时任务
export const editQuartz = (params) => {
    return postRequest('/quartzJob/edit', params)
}
// 暂停定时任务
export const pauseQuartz = (params) => {
    return postRequest('/quartzJob/pause', params)
}
// 恢复定时任务
export const resumeQuartz = (params) => {
    return postRequest('/quartzJob/resume', params)
}
// 删除定时任务
export const deleteQuartz = (ids, params) => {
    return deleteRequest(`/quartzJob/delByIds/${ids}`, params)
}



// 分页获取消息数据
export const getMessageData = (params) => {
    return getRequest('/message/getByCondition', params)
}
// 获取单个消息详情
export const getMessageDataById = (id, params) => {
    return getRequest(`/message/get/${id}`, params)
}
// 添加消息
export const addMessage = (params) => {
    return postRequest('/message/add', params)
}
// 编辑消息
export const editMessage = (params) => {
    return postRequest('/message/edit', params)
}
// 删除消息
export const deleteMessage = (ids, params) => {
    return deleteRequest(`/message/delByIds/${ids}`, params)
}
// 分页获取消息推送数据
export const getMessageSendData = (params) => {
    return getRequest('/messageSend/getByCondition', params)
}
// 编辑发送消息
export const editMessageSend = (params) => {
    return putRequest('/messageSend/update', params)
}
// 删除发送消息
export const deleteMessageSend = (ids, params) => {
    return deleteRequest(`/messageSend/delByIds/${ids}`, params)
}



// 分页获取文件数据
export const getFileListData = (params) => {
    return getRequest('/file/getByCondition', params)
}
// 复制文件
export const copyFile = (params) => {
    return postRequest('/file/copy', params)
}
// 重命名文件
export const renameFile = (params) => {
    return postRequest('/file/rename', params)
}
// 删除文件
export const deleteFile = (ids, params) => {
    return deleteRequest(`/file/delete/${ids}`, params)
}
// 下载文件
export const aliDownloadFile = (fKey, params) => {
    return getRequest(`/file/ali/download/${fKey}`, params)
}


// 检查oss配置
export const checkOssSet = (params) => {
    return getRequest('/setting/oss/check', params)
}
// 获取oss配置
export const getOssSet = (serviceName, params) => {
    return getRequest(`/setting/oss/${serviceName}`, params)
}
// 编辑oss配置
export const editOssSet = (params) => {
    return postRequest('/setting/oss/set', params)
}
// 获取sms配置
export const getSmsSet = (serviceName, params) => {
    return getRequest(`/setting/sms/${serviceName}`, params)
}
// 获取sms模板code
export const getSmsTemplateCode = (type, params) => {
    return getRequest(`/setting/sms/templateCode/${type}`, params)
}
// 编辑sms配置
export const editSmsSet = (params) => {
    return postRequest('/setting/sms/set', params)
}
// 获取email配置
export const getEmailSet = (serviceName, params) => {
    return getRequest('/setting/email', params)
}
// 编辑email配置
export const editEmailSet = (params) => {
    return postRequest('/setting/email/set', params)
}
// 获取vaptcha配置
export const getVaptchaSet = (params) => {
    return getRequest('/setting/vaptcha', params)
}
// 编辑vaptcha配置
export const editVaptchaSet = (params) => {
    return postRequest('/setting/vaptcha/set', params)
}
// 获取vaptcha配置
export const getOtherSet = (params) => {
    return getRequest('/setting/other', params)
}
// 编辑other配置
export const editOtherSet = (params) => {
    return postRequest('/setting/other/set', params)
}
// 查看私密配置
export const seeSecretSet = (settingName, params) => {
    return getRequest(`/setting/seeSecret/${settingName}`, params)
}



// 表格生成
export const generateTable = (name, rowNum, params) => {
    return importRequest(`/generate/table/${name}/${rowNum}`, params)
}
// 树生成
export const generateTree = (name, rowNum, params) => {
    return importRequest(`/generate/tree/${name}/${rowNum}`, params)
}



// base64上传
export const base64Upload = (params) => {
    return postRequest('/upload/file', params)
}

//如下为结算系统API调用
//获取所有支付订单
export const getPayOrders = (params) => {
    return getRequest('/payment/payOrders/getByCondition', params)
}

//获取银行列表
export const getBankListData = (params) => {
    return getRequest('/bank/getByCondition', params)
}
//新增银行
export const setBank = (params) => {
    return postRequest('/bank/save', params)
}
//编辑银行
export const editBank = (params) => {
    return putRequest('/bank/update', params)
}
//删除银行
export const deleteBank = (ids, params) => {
    return deleteRequest(`/bank/delByIds/${ids}`, params)
}
//扣款单
export const getBill = (params) => {
    return getRequest('/bill/getByPage', params)
}
//扣款单全部导出
export const exportBill = (params) => {
    return getRequest('/bill/export', params)
}
//导入扣款单
export const billImportData = (params) => {
    return importRequest('/bill/importData', params)
}
//扣款单搜索
export const getByCondition = (params) => {
    return getRequest('/bill/getByCondition', params)
}
//扣款单弹出层搜索
export const batchGetByCondition = (params) => {
    return getRequest('/batch/getByCondition', params)
}
//扣款结果表
export const billResultGetByCondition = (params) => {
    return getRequest('/billResult/getByCondition', params)
}
//扣款主动还款按钮
export const billSetHasBill = (params) => {
    return getRequest('/bill/setHasBill', params)
}
//扣款结果表全部导出
export const exportBillResult = (params) => {
    return getRequest('/billResult/export', params)
}
//扣款查询明细
export const billGetListById = (params) => {
    return getRequest('/bill/getListById', params)
}

// 放款单搜索
export const lendGetByCondition = (params) => {
    return getRequest('/paymet/getByCondition', params)
}
//放款单全部导出
export const exportPayment = (params) => {
    return getRequest('/paymet/export', params)
}
//放款单全部导出
export const confirmPayment = (params) => {
    return getRequest('/paymet/confirmPayment', params)
}
// //任务列表
// export const taskBillGetByPage = (params) => {
//     return getRequest('/taskBill/getByPage', params)
// }
//任务列表搜索框
export const taskBillGetByCondition = (params) => {
    return getRequest('/task/findByCreateTime', params)
}
//任务列表右侧渲染数据
export const taskBillGetByConditionRight = (params) => {
    return getRequest('/taskBill/getByCondition', params)
}
//任务列表全部导出
export const exportTaskBill = (params) => {
    return getRequest('/taskBill/export', params)
}
//支付渠道
export const thirdPaymentGetAll = (params) => {
    return getRequest('/bizPayment/getByCondition', params)
}
//支付添加渠道
export const thirdPaymentSave = (params) => {
    return postRequest('/bizPayment/saveData', params)
}
//支付渠道新增下拉菜单数据
export const thirdPaymentPayMentList = (params) => {
    return getRequest('/bizPayment/paymentList', params)
}
//编辑支付渠道
export const baofooThirdPaymentUpdateData = (params) => {
    return putRequest('/bizPayment/updateData', params)
}
//删除支付渠道
export const baofooThirdPayment = (params) => {
    return postRequest('/bizPayment/del', params)
}
//支付渠道详情
export const baofooThirdPaymentGetDetail = (params) => {
    return getRequest('/mechant/getByCondition', params)
}
//支付渠道详情新增
export const baofooMerchantSave = (params) => {
    return postRequest('/mechant/save', params)
}
//支付渠道详情编辑
export const baofooMerchantUpdate = (params) => {
    return putRequest('/mechant/update', params)
}
//支付渠道详情删除
export const baofooMerchantDelete = (ids, params) => {
    return deleteRequest(`/mechant/delByIds/${ids}`, params)
}
//支付渠道详情启用禁用
export const baofooMerchantForbidden = (params) => {
    return putRequest('/mechant/forbidden', params)
}
//支付渠道详情 (融宝)
export const reapalThirdPaymentGetDetail = (params) => {
    return getRequest('/reapalMechant/getByCondition', params)
}
//支付渠道详情新增 (融宝)
export const reapalMerchantSave = (params) => {
    return postRequest('/reapalMechant/save', params)
}
//支付渠道详情编辑 (融宝)
export const reapalMerchantUpdate = (params) => {
    return putRequest('/reapalMechant/update', params)
}
//支付渠道详情删除 (融宝)
export const reapalMerchantDelete = (ids, params) => {
    return deleteRequest(`/reapalMechant/delByIds/${ids}`, params)
}
//支付渠道详情启用禁用 (融宝)
export const reapalMerchantForbidden = (params) => {
    return putRequest('/reapalMechant/forbidden', params)
}




//支付渠道详情 (连连)
export const lianlianThirdPaymentGetDetail = (params) => {
    return getRequest('/lianlianMechant/getByCondition', params)
}
//支付渠道详情新增 (连连)
export const lianlianMerchantSave = (params) => {
    return postRequest('/lianlianMechant/save', params)
}
//支付渠道详情编辑 (连连)
export const lianlianMerchantUpdate = (params) => {
    return putRequest('/lianlianMechant/update', params)
}
//支付渠道详情删除 (连连)
export const lianlianMerchantDelete = (ids, params) => {
    return deleteRequest(`/lianlianMechant/delByIds/${ids}`, params)
}
//支付渠道详情启用禁用 (连连)
export const lianlianMerchantForbidden = (params) => {
    return putRequest('/lianlianMechant/forbidden', params)
}







//支付渠道获取银行列表
export const bankGetAll = (ids, params) => {
    return getRequest('/bank/getAll', params)
}
//支付渠道列表数据(技术配置)
export const thirdPaymentGetByCondition = (params) => {
    return getRequest('/thirdPayment/getByCondition', params)
}
//支付渠道新增(技术配置)
export const thirdPaymentSaveData = (params) => {
    return postRequest('/thirdPayment/saveData', params)
}
//支付渠道编辑(技术配置)
export const  thirdPaymentUpdateData= (params) => {
    return putRequest('/thirdPayment/updateData', params)
}
//支付渠道编辑获取银行列表(技术配置)
export const thirdPaymentGet = (ids, params) => {
    return getRequest(`/thirdPayment/get/${ids}`, params)
}
//支付渠道删除(技术配置)
export const thirdPaymentDelByIds = (ids, params) => {
    return deleteRequest(`/thirdPayment/delByIds/${ids}`, params)
}
//支付渠道启用禁用(技术配置)
export const thirdPaymentForbidden = (params) => {
    return putRequest('/thirdPayment/forbidden', params)
}
//支付渠道详情(技术配置)
export const thirdPaymentGetPayDetail = (params) => {
    return getRequest('/bankQuota/getByCondition', params)
}
//支付渠道详情编辑(技术配置)
export const bankQuotaUpdate = (params) => {
    return putRequest('/bankQuota/update', params)
}
//支付渠道详情删除(技术配置)
export const bankQuotaDelByIds = (ids, params) => {
    return deleteRequest(`/bankQuota/delByIds/${ids}`, params)
}
//结算业务方信息 (技术配置)
export const bizGetByCondition = (params) => {
    return getRequest('/biz/getByCondition', params)
}
//结算业务方新增(技术配置)
export const bizSave = (params) => {
    return postRequest('/biz/save', params)
}
//结算业务方编辑(技术配置)
export const bizUpdate = (params) => {
    return putRequest('/biz/update', params)
}
///结算业务方删除(技术配置)
export const bizDelByIds = (ids, params) => {
    return deleteRequest(`/biz/delByIds/${ids}`, params)
}
//结算业务方启用禁用(技术配置)
export const bizForbidden = (params) => {
    return putRequest('/biz/forbidden', params)
}
//支付渠道修改优先级(技术配置)
export const thirdPaymentUpdatePrority = (params) => {
    return putRequest('/thirdPayment/updatePrority', params)
}