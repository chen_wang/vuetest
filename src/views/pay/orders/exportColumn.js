export const exportColumn = [
  {
    title: "批次号",
    key: "batchNo"
  },
  {
    title: "状态",
    key: "billStateDesc"
  },
  {
    title: "渠道-流水号",
    key: "dtOrder"
  },
  {
    title: "业务号",
    key: "bizNo"
  },
  {
    title: "合同号",
    key: "bizAgreementNo"
  },
  {
    title: "客户名",
    key: "customerName"
  },
  {
    title: "应划扣金额（元）",
    key: "amount2"
  },
  {
    title: "已划扣金额（元）",
    key: "hasKillAmount"
  },
  {
    title: "手续费（元）",
    key: "costs"
  },
  {
    title: "创建时间",
    key: "createTime"
  },
  {
    title: "描述",
    key: "description"
  }
]