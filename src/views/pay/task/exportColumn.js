export const exportColumn = [
  {
    title: "合同号",
    key: "bizAgreementNo",
    align: "center"
  },
  {
    title: "订单号",
    key: "orderId",
    align: "center"
  },
  {
    title: "业务方",
    key: "bizName",
    align: "center"
  },
  {
    title: "商户号",
    key: "merchantNo",
    align: "center"
  },
  {
    title: "渠道方",
    key: "channelName",
    align: "center"
  },
  {
    title: "流水号",
    key: "dtOrder",
    align: "center"
  },
  {
    title: "姓名",
    key: "name",
    align: "center"
  },
  {
    title: "银行名称",
    key: "bankName",
    align: "center"
  },
  {
    title: "金额（元）",
    key: "amount2",
    align: "center"
  },
  {
    title: "手续费（元）",
    key: "costs2",
    align: "center"
  },
  {
    title: "划扣时间",
    key: "createTime",
    align: "center"
  },
  {
    title: "划扣状态",
    key: "billStateDesc",
    align: "center"
  },
  {
    title: "状态原因",
    key: "respMsg",
    align: "center"
  }
]