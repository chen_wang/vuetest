export const exportColumn = [{
    title: "业务方",
    key: "bizName"
  },
  {
    title: "合同号",
    key: "bizAgreementNo"
  },
  {
    title: "出款状态",
    key: "payMentStateDesc"
  },
  {
    title: "客户名",
    key: "customerName"
  },
  {
    title: "银行卡号",
    key: "cardNo"
  },
  {
    title: "手机号",
    key: "phone"
  },
  {
    title: "身份证号",
    key: "idNo"
  },
  {
    title: "应出款金额（元）",
    key: "payableAmount"
  },
  {
    title: "已出款金额（元）",
    key: "money"
  },
  {
    title: "渠道",
    key: "thirdPayName"
  },
  {
    title: "商户号",
    key: "merchantNo"
  },
  {
    title: "流水号",
    key: "dtOrder"
  },
  {
    title: "手续费（元）",
    key: "fee"
  },
  {
    title: "创建时间",
    key: "createTime"
  },
  {
    title: "再次审核状态",
    key: "respCode"
  },
  {
    title: "操作日志",
    key: "respMsg"
  }
]