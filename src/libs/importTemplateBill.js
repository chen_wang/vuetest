// 导入表格模版数据
export const userColumns = [
    {
        title: "业务方编号",
        key: "bizNo",
        minWidth: 200,
        align: "center"
    },
    {
        title: "合同编号",
        key: "bizAgreementNo",
        minWidth: 200,
        align: "center"
    },
    {
        title: "商户客户ID",
        key: "bizUserId",
        minWidth: 200,
        align: "center"
    },
    {
        title: "金额",
        key: "amount",
        minWidth: 200,
        align: "center"
    },
    {
        title: "商户号",
        key: "merchantId",
        minWidth: 200,
        align: "center"
    }
]

export const userData = [
   
]