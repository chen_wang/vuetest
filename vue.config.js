module.exports = {
    devServer: {
        host: '127.0.0.1',
        port: 9999,
        proxy: {
            '/gboot': {
                target: 'http://10.0.254.72',  // 请求本地 需要gboot后台项目
                changeOrigin: false,
                ws: false
            },
            '/foo': {
                target: '<other_url>'
            }
        }
    },
    // 打包时不生成.map文件 避免看到源码
    productionSourceMap: false
}